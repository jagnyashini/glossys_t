################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../core/dev/cc2420.c \
../core/dev/glossy.c \
../core/dev/leds.c 

OBJS += \
./core/dev/cc2420.o \
./core/dev/glossy.o \
./core/dev/leds.o 

C_DEPS += \
./core/dev/cc2420.d \
./core/dev/glossy.d \
./core/dev/leds.d 


# Each subdirectory must supply rules for building sources it contributes
core/dev/%.o: ../core/dev/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


