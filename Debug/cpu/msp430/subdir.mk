################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cpu/msp430/clock.c \
../cpu/msp430/leds-arch.c \
../cpu/msp430/loader-arch.c \
../cpu/msp430/msp430.c \
../cpu/msp430/rtimer-arch.c \
../cpu/msp430/spi.c \
../cpu/msp430/watchdog.c 

OBJS += \
./cpu/msp430/clock.o \
./cpu/msp430/leds-arch.o \
./cpu/msp430/loader-arch.o \
./cpu/msp430/msp430.o \
./cpu/msp430/rtimer-arch.o \
./cpu/msp430/spi.o \
./cpu/msp430/watchdog.o 

C_DEPS += \
./cpu/msp430/clock.d \
./cpu/msp430/leds-arch.d \
./cpu/msp430/loader-arch.d \
./cpu/msp430/msp430.d \
./cpu/msp430/rtimer-arch.d \
./cpu/msp430/spi.d \
./cpu/msp430/watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
cpu/msp430/%.o: ../cpu/msp430/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


