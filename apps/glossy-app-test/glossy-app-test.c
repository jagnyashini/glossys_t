/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 *
 * \mainpage
 *           These files document the source code of Glossy, a flooding architecture for wireless sensor networks,
 *           implemented in <a href="http://www.sics.se/contiki/">Contiki</a> based on Tmote Sky sensor nodes.
 *
 *           Glossy was published at ACM/IEEE IPSN '11 in the paper titled
 *           <a href="ftp://ftp.tik.ee.ethz.ch/pub/people/ferrarif/FZTS2011.pdf">
 *           Efficient network flooding and time synchronization with Glossy</a>,
 *           which also received the best paper award.
 *
 *           This documentation is divided into three main parts:
 *           \li \ref glossy-test "Simple application for testing Glossy":
 *           Example of a simple application that periodically floods a packet and prints related statistics.
 *           \li \ref glossy_interface "Glossy API":
 *           API provided by Glossy for an application that wants to use it.
 *           \li \ref glossy_internal "Glossy internal functions":
 *           Functions used internally by Glossy during a flood.
 *
 *           A complete overview of the documentation structure is available <a href="modules.html">here</a>.
 *
 * \author
 *           <a href="http://www.tik.ee.ethz.ch/~ferrarif">Federico Ferrari</a> <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \defgroup glossy-test Simple application for testing Glossy
 *
 *           This application runs Glossy periodically to flood a packet from one node (initiator)
 *           to the other nodes (receivers) and prints flooding-related statistics.
 *
 *           The application schedules Glossy periodically with a fixed period \link GLOSSY_PERIOD \endlink.
 *
 *           The duration of each Glossy phase is given by \link GLOSSY_DURATION \endlink.
 *
 *           During each Glossy phase, the maximum number of transmissions in Glossy (N)
 *           is set to \link N_TX \endlink.
 *
 *           The initiator of the floods is the node having nodeId \link INITIATOR_NODE_ID \endlink.
 *
 *           The packet to be flooded has the format specified by data structure \link glossy_data_struct \endlink.
 *
 *           Receivers synchronize by computing the reference time during each Glossy phase.
 *
 *           To synchronize fast, at startup receivers run Glossy with a significantly shorter period
 *           (\link GLOSSY_INIT_PERIOD \endlink) and longer duration (\link GLOSSY_INIT_DURATION \endlink).
 *
 *           Receivers exit the bootstrapping phase when they have computed the reference time for
 *           \link GLOSSY_BOOTSTRAP_PERIODS \endlink consecutive Glossy phases.
 *
 * @{
 */

/**
 * \file
 *         A simple example of an application that uses Glossy, source file.
 *
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#include "glossy-app-test.h"
#include "cc2420.c"

char app_scheduler(struct rtimer *t, void *ptr);

/**
 * \defgroup glossy-test-variables Application variables
 * @{
 */

/**
 * \defgroup glossy-test-variables-sched-sync Scheduling and synchronization variables
 * @{
 */

static glossy_data_struct glossy_data;     /**< \brief Flooding data. */
static app_data_struct app_data;     /**< \brief Flooding data. */

static struct rtimer rt;                   /**< \brief Rtimer used to schedule Glossy. */
static struct pt pt;                       /**< \brief Protothread used to schedule Glossy. */

static rtimer_clock_t t_ref_l_old = 0;     /**< \brief Reference time computed from the Glossy
                                                phase before the last one. \sa get_t_ref_l */
static uint8_t skew_estimated = 0;         /**< \brief Not zero if the clock skew over a period of length
                                                \link GLOSSY_PERIOD \endlink has already been estimated. */
static uint8_t sync_missed = 0;            /**< \brief Current number of consecutive phases without
                                                synchronization (reference time not computed). */
static rtimer_clock_t t_start = 0;         /**< \brief Starting time (low-frequency clock)
                                                of the last Glossy phase. */
static int period_skew = 0;                /**< \brief Current estimation of clock skew over a period



/** @} */

// New variables

static struct pt app_pt;
static rtimer_clock_t t_app_start = 0;
/**
 * \defgroup glossy-test-variables-stats Statistics variables
 * @{
 */

static unsigned long glossy_packets_received = 0; /**< \brief Current number of received packets. */
static unsigned long glossy_packets_missed = 0;   /**< \brief Current number of missed packets. */
static unsigned long glossy_latency = 0;          /**< \brief Latency of last Glossy phase, in us. */
static unsigned long glossy_sum_latency = 0;      /**< \brief Current sum of latencies, in ticks of low-frequency
                                                clock (used to compute average). */
static unsigned long app_packets_received = 0; /**< \brief Current number of received packets. */
static unsigned long app_packets_missed = 0;   /**< \brief Current number of missed packets. */
static unsigned long app_latency = 0;          /**< \brief Latency of last Glossy phase, in us. */
static unsigned long app_sum_latency = 0;      /**< \brief Current sum of latencies, in ticks of low-frequency

/** @} */
/** @} */

/*Variable declaration for the application*/
#define MAX_NODES 50


#if COOJA
static uint8_t group[][MAX_NODES]={{1,2,3,4},{6,5,7,8}};
#endif

#if INDRIYA
//static uint8_t group[][MAX_NODES]={{30,28,34,27,15,13,11,14,1,20,17,22,18,10,51,52,56,55,46,43,42,48,45,40},{35,32,29,6,5,2,12,7,9,37,19,16,24,54,60,57,61,53,62,38,65,74,44,47}};
//static uint8_t group[][MAX_NODES]={{56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10},{17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34}};

//static uint8_t group[][MAX_NODES]={{2,1,5,7,9,10,12,13,14,15,16,19,27,28,29,30,32,34,35,51,52,56},{40,16,17,18,19,20,22,24,26,37,42,43,44,45,46,47,48,52,53,54,55,60,61}};

//static uint8_t group[][MAX_NODES]={{2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47},{11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40}};


static uint8_t group[][MAX_NODES]={{44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1},{65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9}};

//static uint8_t group[][MAX_NODES]={{2,4,6,8,10,12,14},{5,1,3,7,9,11,13,15}};

#endif

#if FLOCKLAB
//static uint8_t group[][MAX_NODES]={{2,1,3,4,6,8,15,18,22,27,28,31,32,33},{7,11,10,13,14,17,19,20,23,24,25,26}};
//static uint8_t group[][MAX_NODES]={{3,15,8,2,31,6,18,23,24,26,11,25,17,14},{32,33,4,1,28,22,16,27,10,20,19,14,13,7}};

//static uint8_t group[][MAX_NODES]={{26,23,19,17,14,7,24,31,18,6,3,33,8,4},{20,10,25,13,11,28,22,27,16,32,15,2,1}};

//static uint8_t group[][MAX_NODES]={{23,27,18,28,10,31,3,16,15,2,1,17,13,14},{24,10,20,22,19,26,32,33,6,8,4,25,11,7}};

//static uint8_t group[][MAX_NODES]={{17,25,14,7,10,24,18,28,6,32,33,4,1},{26,13,11,23,27,31,22,16,3,15,2,8}};
//static uint8_t group[][MAX_NODES]={{32,28,22,16,8,4,23,27,10,19,17,13,7},{31,3,33,6,15,1,2,24,18,26,20,25,11,14}};

//static uint8_t group[][MAX_NODES]={{2,1,15,32,28,22,16,27,10,26,19,11,17,7},{4,8,33,3,31,6,18,24,23,20,25,14,13}};
//static uint8_t group[][MAX_NODES]={{17,25,20,14,7,27,23,22,28,16,33,8,2},{13,19,26,11,24,10,18,31,32,6,3,15,4,1}};

//static uint8_t group[][MAX_NODES]={{6,22,31,33,8,2,1,27,23,26,19,25,14,11},{16,28,3,32,15,4,18,24,10,20,17,13,7}};
//static uint8_t group[][MAX_NODES]={{20,17,25,11,7,24,27,18,22,6,33,4,8},{19,26,13,14,23,10,28,16,3,15,2,1}};
static uint8_t group[][MAX_NODES]={{11,14,13,19,20,10,27,28,6,16,33,15,2},{7,25,17,26,23,24,18,31,22,3,8,4,1}};
#endif

#if LOCAL
static uint8_t group[][MAX_NODES]={{3,2},{4,1}};
#endif

#define NUMBER_OF_GROUPS sizeof(group)/sizeof(group[0])
static uint16_t sfds[NUMBER_OF_GROUPS]={0xA10F,0xA40F};
static uint8_t new_header[NUMBER_OF_GROUPS]={1,2};
static uint8_t common_header,my_header;
static uint16_t my_group,common_sfd;
static uint16_t my_sfd;
uint16_t reg_app;


/*
printf("%5lu %4u %4lu.%03lu ", glossy_data.seq_no, get_rx_cnt(), latency / 1000, latency % 1000);
printf(" %3lu.%03lu %% ", avg_rel / 1000, avg_rel % 1000);
printf("%4lu,%4lu ", packets_received, packets_missed);
printf("%4lu.%03lu ", avg_radio_on / 1000, avg_radio_on % 1000);
printf("%4lu.%03lu \n", avg_latency / 1000, avg_latency % 1000);
glossy_data.seq_no, get_rx_cnt(), latency, reliability, pkt_received, pkt_missed, avg_on_time, avg_latency
 */

static void print_glossy_stats(){

	if (get_rx_cnt()) {	// Packet received at least once.
		// Increment number of successfully received packets.
		//		glossy_packets_received++;
		//		// Compute latency during last Glossy phase.
		//		rtimer_clock_t lat = get_t_first_rx_l() - get_t_ref_l();
		//		// Add last latency to sum of latencies.
		//		glossy_sum_latency += lat;
		//		// Convert latency to microseconds.
		//		glossy_latency = (unsigned long)(lat) * 1e6 / RTIMER_SECOND;
		// Print information about last packet and related latency.
		//				printf("Glossy received %u time%s: seq_no %lu, latency %lu.%03lu ms\n",
		//						get_rx_cnt(), (get_rx_cnt() > 1) ? "s" : "", glossy_data.seq_no,
		//								latency / 1000, latency % 1000);
		//		printf( "%d %5u %5lu %4u %4lu.%03lu ",initiator_flag, node_id,glossy_data.seq_no, get_rx_cnt(), glossy_latency / 1000, glossy_latency % 1000);
		printf("%5u ",node_id);
		printf("%5u ",glossy_data.seq_no);
		printf("%4u ",get_rx_cnt());
		printf("%5u ",glossy_data.initiator_id);
		uint16_t reg;
		reg = getreg(CC2420_SYNCWORD);

#if(EXP_TYPE == SFD_CHANGE)
		{
			printf("%5u ",reg);						//Outer Glossy SFD
		}
#endif
#if(EXP_TYPE == HEADER_CHANGE)
		{
			printf("%5x ",APPLICATION_HEADER);						//Outer Glossy Headervalue
		}
#endif
#if(EXP_TYPE == WITHOUT_ANY_CHANGE)
		{
			printf("--");						//Outer Glossy SFD
		}
#endif

	} else {	// Packet not received.
		// Increment number of missed packets.
		printf( "%5u %5u %4u %5u %u ", 0,0,0,0,0);
		glossy_packets_missed++;
		// Print failed reception.
		//printf("Glossy NOT received\n");
		//	printf("%5u %5lu %4u %4u ", node_id,0, get_rx_cnt(),0);
		//		printf("%d %5u %5u %4u %4u.%03u ",initiator_flag, node_id,0, 0, 0, 0);


	}
	//#if GLOSSY_DEBUG
	//	//			printf("skew %ld ppm\n", (long)(period_skew * 1e6) / GLOSSY_PERIOD);
	//	printf("high_T_irq %u, rx_timeout %u, bad_length %u, bad_header %u, bad_crc %u\n",
	//			high_T_irq, rx_timeout, bad_length, bad_header, bad_crc);
	//#endif /* GLOSSY_DEBUG */
	//	// Compute current average reliability.
	//	unsigned long avg_rel = glossy_packets_received * 1e5 / (glossy_packets_received + glossy_packets_missed);
	//	// Print information about average reliability.
	//	//			printf("average reliability %3lu.%03lu %% ",
	//	//					avg_rel / 1000, avg_rel % 1000);
	//	//			printf("(missed %lu out of %lu packets)\n",
	//
	//	printf(" %3lu.%03lu %% ", avg_rel / 1000, avg_rel % 1000);
	//	printf("%4lu  %4lu ", glossy_packets_received, glossy_packets_missed);
	//
	//#if ENERGEST_CONF_ON
	//	// Compute average radio-on time, in microseconds.
	//	unsigned long avg_radio_on = (unsigned long)GLOSSY_PERIOD * 1e6 / RTIMER_SECOND *
	//			(energest_type_time(ENERGEST_TYPE_LISTEN) + energest_type_time(ENERGEST_TYPE_TRANSMIT)) /
	//			(energest_type_time(ENERGEST_TYPE_CPU) + energest_type_time(ENERGEST_TYPE_LPM));
	//	// Print information about average radio-on time.
	//	//			printf("average radio-on time %lu.%03lu ms\n",
	//	//					avg_radio_on / 1000, avg_radio_on % 1000);
	//	printf("%4lu.%03lu ", avg_radio_on / 1000, avg_radio_on % 1000);
	//
	//#endif /* ENERGEST_CONF_ON */
	//	// Compute average latency, in microseconds.
	//	unsigned long glossy_avg_latency = glossy_sum_latency * 1e6 / (RTIMER_SECOND * glossy_packets_received);
	//	// Print information about average latency.
	//	//			printf("average latency %lu.%03lu ms\n",
	//	//					avg_latency / 1000, avg_latency % 1000);
	//	printf("%4lu.%03lu ", glossy_avg_latency / 1000, glossy_avg_latency % 1000);

}
static void print_app_stats(){

	if (get_app_rx_cnt()) {	// Packet received at least once.
		// Increment number of successfully received packets.
		app_packets_received++;
		printf("%5u ",initiator_flag);
		printf("%5u ",app_data.app_seq_no);
		printf("%4u ",get_app_rx_cnt());
		printf("%5u ",app_data.initiator_id);
#if(EXP_TYPE == SFD_CHANGE)
		{
			printf("%5u ",reg_app);						//Outer Glossy SFD
		}
#endif
#if(EXP_TYPE == HEADER_CHANGE)
		{
			printf("%5x ",my_header);						//Outer Glossy Headervalue
		}
#endif
#if(EXP_TYPE == WITHOUT_ANY_CHANGE)
		{
			printf("  --");						//Outer Glossy SFD
		}
#endif

	}
	else {	// Packet not received.

		// Increment number of missed packets.
		app_packets_missed++;
		printf( "%5u %5u %4u %5u %5u ", 0,0,0,0,0);

		//		printf("%5u %4u %5u %5u", app_data.app_seq_no, get_app_rx_cnt(),0,0);

	}

	//#if APP_DEBUG
	//	//			printf("skew %ld ppm\n", (long)(period_skew * 1e6) / GLOSSY_PERIOD);
	//	printf("high_T_irq %u, rx_timeout %u, bad_length %u, bad_header %u, bad_crc %u\n",
	//			high_T_irq, rx_timeout, bad_length, bad_header, bad_crc);
	//#endif /* GLOSSY_DEBUG */
	//	// Compute current average reliability.
	//	unsigned long avg_rel = app_packets_received * 1e5 / (app_packets_received + app_packets_missed);
	//	printf(" %3lu.%03lu %% ", avg_rel / 1000, avg_rel % 1000);
	//	printf("%4lu  %4lu ", app_packets_received, app_packets_missed);
	//
	//#if ENERGEST_CONF_ON
	//	// Compute average radio-on time, in microseconds.
	//	// Has to be calculated properly -
	//	unsigned long avg_radio_on = (unsigned long)GLOSSY_PERIOD * 1e6 / RTIMER_SECOND *
	//			(energest_type_time(ENERGEST_TYPE_LISTEN) + energest_type_time(ENERGEST_TYPE_TRANSMIT)) /
	//			(energest_type_time(ENERGEST_TYPE_CPU) + energest_type_time(ENERGEST_TYPE_LPM));
	//	printf("%4lu.%03lu ", avg_radio_on / 1000, avg_radio_on % 1000);
	//
	//#endif /* ENERGEST_CONF_ON */
	//	// Compute average latency, in microseconds.
	//	unsigned long app_avg_latency = app_sum_latency * 1e6 / (RTIMER_SECOND * app_packets_received);
	//	printf("%4lu.%03lu", app_avg_latency / 1000, app_avg_latency % 1000);


}


PROCESS(print_stats_process, "print stats");
PROCESS_THREAD(print_stats_process, ev, data)
{
	PROCESS_BEGIN();

	while(1) {
		PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
		// Print statistics only if Glossy is not still bootstrapping.

		if (!GLOSSY_IS_BOOTSTRAPPING()) {


			print_glossy_stats();

			printf(" |");

			print_app_stats();

			printf("\n");

		}
	}

	PROCESS_END();
}

/** @} */

/**
 * \defgroup glossy-test-skew Clock skew estimation
 * @{
 */

static inline void estimate_period_skew(void) {
	// Estimate clock skew over a period only if the reference time has been updated.
	if (GLOSSY_IS_SYNCED()) {
		// Estimate clock skew based on previous reference time and the Glossy period.
		period_skew = get_t_ref_l() - (t_ref_l_old + (rtimer_clock_t)GLOSSY_PERIOD);
		// Update old reference time with the newer one.
		t_ref_l_old = get_t_ref_l();
		// If Glossy is still bootstrapping, count the number of consecutive updates of the reference time.
		if (GLOSSY_IS_BOOTSTRAPPING()) {
			// Increment number of consecutive updates of the reference time.
			skew_estimated++;
			// Check if Glossy has exited from bootstrapping.
			if (!GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy has exited from bootstrapping.
				leds_off(LEDS_RED);
				// Initialize Energest values.
				energest_init();
#if GLOSSY_DEBUG
				high_T_irq = 0;
				bad_crc = 0;
				bad_length = 0;
				bad_header = 0;
#endif /* GLOSSY_DEBUG */

			}
		}
	}
}

/** @} */

/**
 * \defgroup glossy-test-scheduler Periodic scheduling
 * @{
 */

char glossy_scheduler(struct rtimer *t, void *ptr) {
	PT_BEGIN(&pt);

	if (IS_INITIATOR()) {	// Glossy initiator.
		while (1) {
			// Increment sequence number.
			glossy_data.seq_no++;
			glossy_data.initiator_id=node_id;
			// Glossy phase.
			leds_on(LEDS_GREEN);
			rtimer_clock_t t_stop = RTIMER_TIME(t) + GLOSSY_DURATION;
			// Start Glossy.
			glossy_start((uint8_t *)&glossy_data, DATA_LEN, GLOSSY_INITIATOR, GLOSSY_SYNC, N_TX,
					APPLICATION_HEADER, t_stop, (rtimer_callback_t)glossy_scheduler, t, ptr);
			// Store time at which Glossy has started.
			t_start = RTIMER_TIME(t);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&pt);

			// Off phase.
			leds_off(LEDS_GREEN);
			// Stop Glossy.
			glossy_stop();
			if (!GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy has already successfully bootstrapped.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated: increment reference time by GLOSSY_PERIOD.
					set_t_ref_l(GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD);
					set_t_ref_l_updated(1);

				}
				if (IS_APP_INITIATOR()) {	// App initiator.

					rtimer_set(t, t_start + GLOSSY_DURATION + GAP, 1,
							(rtimer_callback_t)app_scheduler, ptr);


				} else {

					//Glossy has already successfully bootstrapped:
					rtimer_set(t, t_start + (GLOSSY_DURATION+GAP) +
							period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
							(rtimer_callback_t)app_scheduler, ptr);

				}

				controller = CNTRL_APP;
				// After  RTIMER_TIME/128 tick go and execute the app.
				// This much time is necessary so that it can execute the estimate_period_skew
				// and others below
				// You should schedule the gloss from app

			} else {

				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);
				// if still bootstrapping
			}

			// Schedule begin of next Glossy phase based on GLOSSY_PERIOD.
			// Estimate the clock skew over the last period.
			estimate_period_skew();

			// Yield the protothread.
			PT_YIELD(&pt);
		}
	} else {	// Glossy receiver.
		while (1) {
			// Glossy phase.
			leds_on(LEDS_GREEN);
			rtimer_clock_t t_stop;
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrapping:
				// Schedule end of Glossy phase based on GLOSSY_INIT_DURATION.
				t_stop = RTIMER_TIME(t) + GLOSSY_INIT_DURATION;
			} else {
				// Glossy has already successfully bootstrapped:
				// Schedule end of Glossy phase based on GLOSSY_DURATION.
				t_stop = RTIMER_TIME(t) + GLOSSY_DURATION;
			}
			// Start Glossy.
			glossy_start((uint8_t *)&glossy_data, DATA_LEN, GLOSSY_RECEIVER, GLOSSY_SYNC, N_TX,
					APPLICATION_HEADER, t_stop, (rtimer_callback_t)glossy_scheduler, t, ptr);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&pt);

			// Off phase.
			leds_off(LEDS_GREEN);
			// Stop Glossy.
			glossy_stop();
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrapping.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated: reset skew_estimated to zero.
					skew_estimated = 0;
				}
			} else {
				// Glossy has already successfully bootstrapped.
				if (!GLOSSY_IS_SYNCED()) {
					// The reference time was not updated:
					// increment reference time by GLOSSY_PERIOD + period_skew.
					set_t_ref_l(GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD + period_skew);
					set_t_ref_l_updated(1);
					// Increment sync_missed.
					sync_missed++;
				} else {
					// The reference time was not updated: reset sync_missed to zero.
					sync_missed = 0;
				}
			}
			// Estimate the clock skew over the last period.
			estimate_period_skew();
			if (GLOSSY_IS_BOOTSTRAPPING()) {
				// Glossy is still bootstrapping.
				if (skew_estimated == 0) {
					// The reference time was not updated:
					// Schedule begin of next Glossy phase based on last begin and GLOSSY_INIT_PERIOD.
					rtimer_set(t, RTIMER_TIME(t) + GLOSSY_INIT_PERIOD, 1,
							(rtimer_callback_t)glossy_scheduler, ptr);
				} else {
					// The reference time was updated:
					// Schedule begin of next Glossy phase based on reference time and GLOSSY_INIT_PERIOD.
					rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD - GLOSSY_INIT_GUARD_TIME, 1,
							(rtimer_callback_t)glossy_scheduler, ptr);
				}
			} else {



				if (IS_APP_INITIATOR()) {	// App initiator.

					rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_DURATION + GAP, 1,
							(rtimer_callback_t)app_scheduler, ptr);

				} else {

					//Glossy has already successfully bootstrapped:
					rtimer_set(t, GLOSSY_REFERENCE_TIME + (GLOSSY_DURATION+GAP) +
							period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
							(rtimer_callback_t)app_scheduler, ptr);

				}
				controller = CNTRL_APP;

			}
			// Yield the protothread.
			PT_YIELD(&pt);

		}
	}

	PT_END(&pt);
}


char app_scheduler(struct rtimer *t, void *ptr) {
	PT_BEGIN(&app_pt);
	uint8_t app_header;
	if (IS_APP_INITIATOR()) {	// App initiator.
		while (1) {
			// Increment sequence number.

			//glossy_data.seq_no++;
			app_data.initiator_id=node_id;
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_app_stop = RTIMER_TIME(t) + APP_DURATION;
#if (EXP_TYPE==SFD_CHANGE)
			set_syncword(my_sfd);
			app_header=APPLICATION_HEADER;

#endif
#if (EXP_TYPE==HEADER_CHANGE)
			app_header=my_header;
			//change with my_header
#endif
			// Start Glossy.
			reg_app = getreg(CC2420_SYNCWORD);

			app_start((uint8_t *)&app_data,APP_DATA_LEN, APP_INITIATOR, 0, APP_N_TX,
					app_header, t_app_stop, (rtimer_callback_t)app_scheduler, t, ptr);
			// Store time at which Glossy has started.
			t_app_start = RTIMER_TIME(t);
			// Yield the protothread. It will be resumed when Glossy terminates.
			PT_YIELD(&app_pt);

			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop Glossy.
			app_stop();
#if (EXP_TYPE==SFD_CHANGE)
			set_syncword(common_sfd);
#endif
#if (EXP_TYPE==HEADER_CHANGE)

			//DO nothing

#endif
			if(IS_INITIATOR()){
				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);
			}else {
				rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD +
						period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
						(rtimer_callback_t)glossy_scheduler, ptr);
			}
			//			rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD +
			//								period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
			//								(rtimer_callback_t)glossy_scheduler, ptr);
			controller = CNTRL_GLOSSY;
			process_poll(&print_stats_process);

			// Yield the protothread.
			PT_YIELD(&app_pt);
		}
	} else {	// App receiver.
		while (1) {


			// Glossy phase.
			leds_on(LEDS_BLUE);
			rtimer_clock_t t_app_stop = RTIMER_TIME(t) + APP_DURATION;
#if (EXP_TYPE==SFD_CHANGE)
			set_syncword(my_sfd);
			app_header=APPLICATION_HEADER;
#endif
#if (EXP_TYPE==HEADER_CHANGE)
			app_header=my_header;
			//change with my_header
#endif
			// Start APP.
			reg_app = getreg(CC2420_SYNCWORD);
			app_start((uint8_t *)&app_data, APP_DATA_LEN, APP_RECEIVER, 0, APP_N_TX,
					app_header, t_app_stop, (rtimer_callback_t)app_scheduler, t, ptr);

			// Yield the protothread. It will be resumed when App terminates.
			PT_YIELD(&app_pt);

			// Off phase.
			leds_off(LEDS_BLUE);
			// Stop App.
			app_stop();
#if (EXP_TYPE==SFD_CHANGE)
			set_syncword(common_sfd);
#endif
#if (EXP_TYPE==HEADER_CHANGE)

			//Do nothing
#endif
			if(IS_INITIATOR()){

				rtimer_set(t, t_start + GLOSSY_PERIOD, 1, (rtimer_callback_t)glossy_scheduler, ptr);

			}else {

				rtimer_set(t, GLOSSY_REFERENCE_TIME + GLOSSY_PERIOD +
						period_skew - GLOSSY_GUARD_TIME * (1 + sync_missed), 1,
						(rtimer_callback_t)glossy_scheduler, ptr);
			}
			controller = CNTRL_GLOSSY;

			process_poll(&print_stats_process);

			// Yield the protothread.
			PT_YIELD(&app_pt);

		}
	}

	PT_END(&app_pt);
}


/** @} */

/**
 * \defgroup glossy-test-init Initialization
 * @{
 */

PROCESS(glossy_test, "Glossy test");
AUTOSTART_PROCESSES(&glossy_test);
PROCESS_THREAD(glossy_test, ev, data)
{
	PROCESS_BEGIN();

	leds_on(LEDS_RED);
	// Initialize Glossy data.
	glossy_data.seq_no = 0;
	app_data.app_seq_no = 0;


	// Find out your SFD based on the group you belong to.
	uint8_t grp_ptr=0,node_ptr=0;
	for(grp_ptr=0;grp_ptr<NUMBER_OF_GROUPS;grp_ptr++)
	{
		for(node_ptr=0;node_ptr< MAX_NODES;node_ptr++)
		{
			if(group[grp_ptr][node_ptr]==node_id)
			{
				my_group = grp_ptr;
			}
		}
	}
	if(node_id==group[my_group][0])
	{
		initiator_flag=1;
	}

#if (EXP_TYPE==SFD_CHANGE)
	// Set the variable my_sfd with this
	my_sfd = sfds[my_group];

	// Set the variable common_sfd here also.
	common_sfd=0xA70F;
#endif

#if (EXP_TYPE==HEADER_CHANGE)
	my_header = new_header[my_group];
#endif

	// Start print stats processes.
	process_start(&print_stats_process, NULL);
	// Start Glossy busy-waiting process.
	process_start(&glossy_process, NULL);
	process_start(&app_process, NULL);
	// Start Glossy experiment in one second.
	rtimer_set(&rt, RTIMER_NOW() + RTIMER_SECOND, 1, (rtimer_callback_t)glossy_scheduler, NULL);
	controller = CNTRL_GLOSSY;

	PROCESS_END();
}

/** @} */
/** @} */
/** @} */
