/*
 * Copyright (c)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Sudipta Saha <sudipta@iitbbs.ac.in>
 *
 */

/**
 * \file
 *         App core, source file.
 * \author
 *         Sudipta Saha <sudipta@iitbbs.ac.in>
 */

#include "app.h"
#include "glossy.h"




//#define CM_POS              CM_1
//#define CM_NEG              CM_2
//#define CM_BOTH             CM_3
//
static uint8_t app_initiator, app_sync, app_rx_cnt, app_tx_cnt, app_tx_max;
static uint8_t *app_data, *app_packet;
static uint8_t app_data_len, app_packet_len, app_packet_len_tmp, app_header;
static uint8_t app_bytes_read, app_tx_relay_cnt_last, app_n_timeouts;
static volatile uint8_t app_state;
static rtimer_clock_t app_t_rx_start, app_t_rx_stop, app_t_rx_stop_tmp, app_t_tx_start, app_t_tx_stop;
static rtimer_clock_t app_t_rx_timeout, app_rx_timeout;
static rtimer_clock_t app_T_irq;
static rtimer_clock_t app_t_stop, app_t_start;
static rtimer_callback_t app_cb;
static struct rtimer *app_rtimer;
static void *app_ptr;
//static unsigned short ie1, ie2, p1ie, p2ie, tbiv;
static unsigned short app_tbiv;

static rtimer_clock_t app_T_slot_h, app_T_rx_h, app_T_w_rt_h, app_T_tx_h, app_T_w_tr_h, app_t_ref_l, app_T_offset_h, app_t_first_rx_l;

static uint8_t app_relay_cnt, app_t_ref_l_updated;

inline void state_machine_app(unsigned short app_tbiv_){
	// read TBIV to clear IFG
	//app_tbiv = TBIV;

	app_tbiv = app_tbiv_;

	if (app_state == APP_STATE_WAITING && SFD_IS_1) {
//			// packet reception has started
		app_begin_rx();
	} else {
		if (app_state == APP_STATE_RECEIVED && SFD_IS_1) {
//				// packet transmission has started
			app_begin_tx();
		} else {
			if (app_state == APP_STATE_TRANSMITTING && !SFD_IS_1) {
				// packet transmission has finished
				app_end_tx();
			} else {
				if (app_state == APP_STATE_ABORTED) {
					// packet reception has been aborted
					app_state = APP_STATE_WAITING;
				} else {
					if ((app_state == APP_STATE_WAITING) && (app_tbiv == TBIV_TBCCR4)) {

						// initiator timeout
						app_n_timeouts++;
						if (app_rx_cnt == 0) {
							// no packets received so far: send the packet again
							app_tx_cnt = 0;
							// set the packet length field to the appropriate value
							APP_LEN_FIELD = app_packet_len_tmp;
//								// set the header field
							APP_HEADER_FIELD = APP_HEADER | (app_header & ~APP_HEADER_MASK);
//								if (app_sync) {
//									APP_RELAY_CNT_FIELD = app_n_timeouts * APP_INITIATOR_TIMEOUT;
//								}
							// copy the application data to the data field
							memcpy(&APP_DATA_FIELD, app_data, app_data_len);
//								// set Glossy state
							app_state = APP_STATE_RECEIVED;
							state_radio = STATE_RADIO_TRANSMITTING;
//								// write the packet to the TXFIFO
							radio_write_tx(app_packet, app_packet_len_tmp);
							// start another transmission
							radio_start_tx();
//								// schedule the timeout again
							app_schedule_initiator_timeout();
						} else {
//								// at least one packet has been received: just stop the timeout
							app_stop_initiator_timeout();
						}
					} else {
						if (app_tbiv == TBIV_TBCCR5) {
//								// rx timeout
							if (app_state == APP_STATE_RECEIVING) {
								// we are still trying to receive a packet: abort the reception
								radio_abort_rx();
								app_state = APP_STATE_WAITING;
								state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
								app_rx_timeout++;
#endif /* GLOSSY_DEBUG */
							}
//								// stop the timeout
							glossy_stop_rx_timeout();
						} else {
							if (app_state != APP_STATE_OFF) {
//									// something strange is going on: go back to the waiting state
								radio_flush_rx();
								app_state = APP_STATE_WAITING;
								state_radio = STATE_RADIO_WAITING;
							}
						}
					}
				}
			}
		}
	}
}

/* --------------------------- Glossy process ----------------------- */
PROCESS(app_process, "App busy-waiting process");
PROCESS_THREAD(app_process, ev, data) {
	PROCESS_BEGIN();

	do {
		app_packet = (uint8_t *) malloc(128);
    } while (app_packet == NULL);

	while (1) {
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);
		// prevent the Contiki main cycle to enter the LPM mode or
		// any other process to run while Glossy is running
		while (APP_IS_ON() && RTIMER_CLOCK_LT(RTIMER_NOW(), app_t_stop));

#if COOJA

		while (app_state == APP_STATE_TRANSMITTING);
#endif /* COOJA */
		// Glossy finished: execute the callback function
		dint();
		app_cb(app_rtimer, app_ptr);
		eint();
	}

	PROCESS_END();
}


/* --------------------------- Main interface ----------------------- */
void app_start(uint8_t *app_data_, uint8_t app_data_len_, uint8_t app_initiator_,
		uint8_t app_sync_, uint8_t app_tx_max_, uint8_t app_header_,
		rtimer_clock_t app_t_stop_, rtimer_callback_t app_cb_,
		struct rtimer *app_rtimer_, void *app_ptr_) {
	// copy function arguments to the respective Glossy variables
	app_data = app_data_;
	app_data_len = app_data_len_;
	app_initiator = app_initiator_;
	//app_sync = app_sync_;
	app_tx_max = app_tx_max_;
	app_header = app_header_;

	app_t_stop = app_t_stop_;
	app_cb = app_cb_;
	app_rtimer = app_rtimer_;
	app_ptr = app_ptr_;

	// disable all interrupts that may interfere with Glossy
	glossy_disable_other_interrupts();
//	// initialize Glossy variables
	app_tx_cnt = 0;
	app_rx_cnt = 0;

	app_t_start = RTIMER_NOW_DCO();
	// set Glossy packet length, with or without relay counter depending on the sync flag value
	if (app_data_len) {
//		app_packet_len_tmp = (sync) ?
//				app_data_len + APP_FOOTER_LEN + APP_RELAY_CNT_LEN + APP_HEADER_LEN :
//				app_data_len + APP_FOOTER_LEN + APP_HEADER_LEN;
		app_packet_len_tmp = app_data_len + APP_FOOTER_LEN + APP_HEADER_LEN;
		app_packet_len = app_packet_len_tmp;
//		// set the packet length field to the appropriate value
		APP_LEN_FIELD = app_packet_len_tmp;
		// set the header field
		APP_HEADER_FIELD = APP_HEADER | (app_header & ~APP_HEADER_MASK);
	} else {
		// packet length not known yet (only for receivers)
		app_packet_len = 0;
	}
	if (app_initiator) {
		// initiator: copy the application data to the data field
		memcpy(&APP_DATA_FIELD, app_data, app_data_len);
		// set Glossy state
		app_state = APP_STATE_RECEIVED;
		state_radio = STATE_RADIO_TRANSMITTING;

	} else {
		// receiver: set Glossy state
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;

	}
//	if (sync) {
//		// set the relay_cnt field to 0
//		GLOSSY_RELAY_CNT_FIELD = 0;
//		// the reference time has not been updated yet
//		t_ref_l_updated = 0;
//	}

#if !COOJA
	// resynchronize the DCO
	msp430_sync_dco();
#endif /* COOJA */

	// flush radio buffers
	radio_flush_rx();
	radio_flush_tx();
	if (app_initiator) {
		// write the packet to the TXFIFO
		radio_write_tx(app_packet, app_packet_len_tmp);
		// start the first transmission
		radio_start_tx();
		// schedule the initiator timeout
		//if ((!sync) || app_T_slot_h) {
			app_n_timeouts = 0;
			app_schedule_initiator_timeout();
		//}
	} else {
		// turn on the radio
		radio_on();
	}
	// activate the Glossy busy waiting process
	process_poll(&app_process);
}

uint8_t app_stop(void) {
	// stop the initiator timeout, in case it is still active
	app_stop_initiator_timeout();
	// turn off the radio
	radio_off();
//
//	// flush radio buffers
	radio_flush_rx();
	radio_flush_tx();
//
	app_state = APP_STATE_OFF;
	state_radio = STATE_RADIO_WAITING;
//	// re-enable non Glossy-related interrupts
	glossy_enable_other_interrupts();
	// return the number of times the packet has been received
	return app_rx_cnt;
}

uint8_t get_app_rx_cnt(void) {
	return app_rx_cnt;
}
//
uint8_t get_app_relay_cnt(void) {
	return app_relay_cnt;
}
//
rtimer_clock_t get_app_T_slot_h(void) {
	return app_T_slot_h;
}

uint8_t get_app_state(void) {
	return app_state;
}


///* ----------------------- Interrupt functions ---------------------- */
inline void app_begin_rx(void) {
	app_t_rx_start = TBCCR1;
	app_state = APP_STATE_RECEIVING;
	state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
	if (app_packet_len) {
		// Rx timeout: packet duration + 200 us
		// (packet duration: 32 us * packet_length, 1 DCO tick ~ 0.23 us)
		app_t_rx_timeout = app_t_rx_start + ((rtimer_clock_t)app_packet_len_tmp * 35 + 200) * 4;
	}
//
	// wait until the FIFO pin is 1 (i.e., until the first byte is received)
	while (!FIFO_IS_1) {
		if (app_packet_len && !RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_t_rx_timeout)) {
			radio_abort_rx();
			app_state = APP_STATE_WAITING;
			state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
			app_rx_timeout++;
#endif /* GLOSSY_DEBUG */
			return;
		}
	};
	// read the first byte (i.e., the len field) from the RXFIFO
	FASTSPI_READ_FIFO_BYTE(APP_LEN_FIELD);
	// keep receiving only if it has the right length
	if ((app_packet_len && (APP_LEN_FIELD != app_packet_len_tmp))
			|| (APP_LEN_FIELD < APP_FOOTER_LEN) || (APP_LEN_FIELD > 127)) {
		// packet with a wrong length: abort packet reception
		radio_abort_rx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
		app_bad_length++;
#endif /* GLOSSY_DEBUG */
		return;
	}
	app_bytes_read = 1;
	if (!app_packet_len) {
		app_packet_len_tmp = APP_LEN_FIELD;
		app_t_rx_timeout = app_t_rx_start + ((rtimer_clock_t)app_packet_len_tmp * 35 + 200) * 4;
	}

#if !COOJA
	// wait until the FIFO pin is 1 (i.e., until the second byte is received)
	while (!FIFO_IS_1) {
		if (!RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_t_rx_timeout)) {

			radio_abort_rx();
			app_state = APP_STATE_WAITING;
			state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
			app_rx_timeout++;
#endif /* GLOSSY_DEBUG */
			return;
		}
	};
	// read the second byte (i.e., the header field) from the RXFIFO
	FASTSPI_READ_FIFO_BYTE(APP_HEADER_FIELD);
	uint8_t app_header_temp;
	app_header_temp =APP_HEADER_FIELD & ~APP_HEADER_MASK;
	// keep receiving only if it has the right header
	if ((APP_HEADER_FIELD & APP_HEADER_MASK) != APP_HEADER && app_header_temp!=app_header) {
		// packet with a wrong header: abort packet reception
		radio_abort_rx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
		app_bad_header++;
#endif /* GLOSSY_DEBUG */
		return;
	}
	app_bytes_read = 2;
	if (app_packet_len_tmp > 8) {
		// if packet is longer than 8 bytes, read all bytes but the last 8
		while (app_bytes_read <= app_packet_len_tmp - 8) {
			// wait until the FIFO pin is 1 (until one more byte is received)
			while (!FIFO_IS_1) {
				if (!RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), app_t_rx_timeout)) {
					radio_abort_rx();
					app_state = APP_STATE_WAITING;
					state_radio = STATE_RADIO_WAITING;

#if APP_DEBUG
					app_rx_timeout++;
#endif /* GLOSSY_DEBUG */
					return;
				}
			};
			// read another byte from the RXFIFO
			FASTSPI_READ_FIFO_BYTE(app_packet[app_bytes_read]);
			app_bytes_read++;
		}
	}
#endif /* COOJA */
	glossy_schedule_rx_timeout(app_t_rx_timeout);
}

inline void app_end_rx(void) {
	rtimer_clock_t app_t_rx_stop_tmp = TBCCR1;
	// read the remaining bytes from the RXFIFO
	FASTSPI_READ_FIFO_NO_WAIT(&app_packet[app_bytes_read], app_packet_len_tmp - app_bytes_read + 1);
	app_bytes_read = app_packet_len_tmp + 1;
#if COOJA
	if ((APP_CRC_FIELD & APP_FOOTER1_CRC_OK) && ((APP_HEADER_FIELD & APP_HEADER_MASK) == APP_HEADER)) {
#else
	if (APP_CRC_FIELD & APP_FOOTER1_CRC_OK) {
#endif /* COOJA */
		app_header = APP_HEADER_FIELD & ~APP_HEADER_MASK;
//		// packet correctly received
//		if (sync) {
//			// increment relay_cnt field
//			APP_RELAY_CNT_FIELD++;
//		}
		if (app_tx_cnt == app_tx_max) {
			// no more Tx to perform: stop Glossy
			radio_off();
			app_state = APP_STATE_OFF;
			state_radio = STATE_RADIO_WAITING;
		} else {
			// write Glossy packet to the TXFIFO
			radio_write_tx(app_packet, app_packet_len_tmp);
			app_state = APP_STATE_RECEIVED;
			state_radio = STATE_RADIO_TRANSMITTING;
		}
		if (app_rx_cnt == 0) {
			// first successful reception:
			// store current time and received relay counter
			app_t_first_rx_l = RTIMER_NOW();
//			if (app_sync) {
//				app_relay_cnt = APP_RELAY_CNT_FIELD - 1;
//			}
		}
		app_rx_cnt++;
//		if (app_sync) {
//			estimate_slot_length(t_rx_stop_tmp);
//		}
		app_t_rx_stop = app_t_rx_stop_tmp;
		if (app_initiator) {
			// a packet has been successfully received: stop the initiator timeout
			app_stop_initiator_timeout();
		}
		if (!app_packet_len) {
			app_packet_len = app_packet_len_tmp;
//			app_data_len = (app_sync) ?
//					app_packet_len_tmp - APP_FOOTER_LEN - APP_RELAY_CNT_LEN - APP_HEADER_LEN :
//					app_packet_len_tmp - APP_FOOTER_LEN - APP_HEADER_LEN;
			app_data_len = app_packet_len_tmp - APP_FOOTER_LEN - APP_HEADER_LEN;
		}
	} else {
#if APP_DEBUG
		app_bad_crc++;
#endif /* GLOSSY_DEBUG */
		// packet corrupted, abort the transmission before it actually starts
		radio_abort_tx();
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;
	}
}
//
inline void app_begin_tx(void) {
	app_t_tx_start = TBCCR1;
	app_state = APP_STATE_TRANSMITTING;
	state_radio = STATE_RADIO_TRANSMITTING_NORMAL;
	app_tx_relay_cnt_last = APP_RELAY_CNT_FIELD;
	if ((!app_initiator) && (app_rx_cnt == 1)) {
		// copy the application data from the data field
		memcpy(app_data, &APP_DATA_FIELD, app_data_len);
	}
//	if ((app_sync) && (app_T_slot_h) && (!app_t_ref_l_updated) && (app_rx_cnt)) {
//		// compute the reference time after the first reception (higher accuracy)
//		compute_sync_reference_time();
//	}

}
//
inline void app_end_tx(void) {
	ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
	ENERGEST_ON(ENERGEST_TYPE_LISTEN);
	app_t_tx_stop = TBCCR1;
	// stop Glossy if tx_cnt reached tx_max (and tx_max > 1 at the initiator)
	if ((++app_tx_cnt == app_tx_max) && ((app_tx_max - app_initiator) > 0)) {
		radio_off();
		app_state = APP_STATE_OFF;
		state_radio = STATE_RADIO_WAITING;
	} else {
		app_state = APP_STATE_WAITING;
		state_radio = STATE_RADIO_WAITING;
	}
	radio_flush_tx();
}
//

inline void app_schedule_initiator_timeout() {
#if !COOJA
	TBCCR4 = app_t_start + (app_n_timeouts + 1) * APP_INITIATOR_TIMEOUT *
			((rtimer_clock_t)app_packet_len * 35 + 400) * 4;

	TBCCTL4 = CCIE;
#endif
}

inline void app_stop_initiator_timeout(void) {
	TBCCTL4 = 0;
}
