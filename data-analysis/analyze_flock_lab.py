import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	
	#group1=[2,1,15,32,28,22,16,27,10,26,19,11,17,7]
	#group2=[4,8,33,3,31,6,18,24,23,20,25,14,13]
	#node_set = ['2','1','15','32','28','22','16','27','10','26','19','11','17','7','4','8','33','3','31','6','18','24','23','20','25','14','13']

#	group1=[20,17,25,11,7,24,27,18,22,6,33,4,8]
#	group2=[19,26,13,14,23,10,28,16,3,15,2,1]
#	node_set = ['20','17','25','11','7','24','27','18','22','6','33','4','8',19,'26','13','14','23','10','28','16','3','15','2','1']
	
	#group1=[17,25,20,14,7,27,23,22,28,16,33,8,2]
	#group2=[13,19,26,11,24,10,18,31,32,6,3,15,4,1]
	#node_set = ['17','25','20','14','7','27','23','22','28','16','33','8','2','13','19','26','11','24','10','18','31','32','6','3','15','4','1']

	#group1=[6,22,31,33,8,2,1,27,23,26,19,25,14,11]
	#group2=[16,28,3,32,15,4,18,24,10,20,17,13,7]
	#node_set = ['6','22','31','33','8','2','1','27','23','26','19','25','14','11','16','28','3','32','15','4','18','24','10','20','17','13','7']

	#group1=[17,25,14,7,10,24,18,28,6,32,33,4,1]
	#group2=[26,13,11,23,27,31,22,16,3,15,2,8]
	#node_set = ['17','25','14','7','10','24','18','28','6','32','33','4','1','26','13','11','23','27','31','22','16','3','15','2','8']


	group2=[11,14,13,19,20,10,27,28,6,16,33,15,2]
	group1=[7,25,17,26,23,24,18,31,22,3,8,4,1]
	node_set = ['11','14','13','19','20','10','27','28','6','16','33','15','2','7','25','17','26','23','24','18','31','22','3','8','4','1']

#	group1=[2,1,3,4,6,8,15,18,22,27,28,31,32,33]
#	group2=[7,11,10,13,14,17,19,20,23,24,25,26]
#	node_set =['2','1','3','4','6','8','15','18','22','27','28','31','32','33','7','11','10','13','14','17','19','20','23','24','25','26']
	group2_initiator=group2[0]
	group1_initiator=group1[0]
	initiator2=group2[0]
	initiator1=group1[0]


	f = []
        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD"
        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print"Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t\t\t""No of packets reception from Self Group_Ini\t""No of packets reception from other GroupIni"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/WithSFD/71333'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/WithSFD/71333',name)

			#print(fullName)

				#-----------------------------------------------------------------
				# For removing the NULL characters
#			f = open(fullName, 'rb')
#			data = f.read()
#			f.close()

#			f = open(fullName, 'wb')

#			f.write(data.replace('nodeid', ''))
#			f.write(data.replace('name', ''))
#			f.write(data.replace('time', ''))
#			f.write(data.replace('value', ''))
#			f.close()
				#-----------------------------------------------------------------

			node_list = []
			f = open(fullName, 'r')
			i=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#if(columns[3]!=str2):
				try:
			    	#	gotdata = columns[1]
					#print(columns)
					node_list.append((columns[1]))
				except IndexError:
			    		gotdata = 'null'
				#print(columns)
				#print(columns[2])
				#node_list.append((columns[1]))
				#i=i+1
				#print(columns[1])
			#	print(columns[2])
				#print(columns[3])
				#print(columns[4])
					#columns[0]	node id
					#columns[1]
					#columns[2]
					#columns[3]
					#columns[4]
					#columns[5]

 			f.close()

			node_set1 = set(node_list)
			#print len(node_set)
			#print sorted(node_set)
			total2=0
			initiators=[]
			node_list_1=[]
			node_list_2=[]
			node_list_3=[]
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'

					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (count_initiator1,count_initiator2)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group1_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t\t",round(percent_initiator2,1),"\t\t\t\t\t",count_initiator1,"\t\t\t\t\t",count_initiator2
				if (group_ini==group1_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
							
						#if(count_initiator1==total2):
						#	node_list_1.append(columns[1])
						#	count_node_1=count_node_1+1
						#if(count_initiator2==total2):
						#	node_list_2.append(columns[1])
						#	count_node_2=count_node_2+1										
						#if((count_initiator1!=total2) and (count_initiator2!=total2)):
						#	node_list_3.append(columns[1])
						#	count_node_both=count_node_both+1
					#print(columns)

			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				total2=0
				total=0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'
					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (i,count_initiator1,count_initiator2,flag)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group2_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t\t",round(percent_initiator1,1),"\t\t\t\t\t",count_initiator2,"\t\t\t\t\t",count_initiator1
				if (group_ini==group2_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
				
	 			f.close()
#				node_neighbor_set = set(node_neighbor_list)
#				print i,":"
#				print sorted(node_neighbor_set)
#				node_neighbor_list = []

        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print"Without SDF"
        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print"Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t\t\t""No of packets reception from Self Group_Ini\t""No of packets reception from other GroupIni"
	f1=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/Nochange/71331'):

		f1.extend(filenames)
		for name in f1:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/Nochange/71331',name)

			#print(fullName)

				#-----------------------------------------------------------------
				# For removing the NULL characters
#			f = open(fullName, 'rb')
#			data = f.read()
#			f.close()

#			f = open(fullName, 'wb')

#			f.write(data.replace('nodeid', ''))
#			f.write(data.replace('name', ''))
#			f.write(data.replace('time', ''))
#			f.write(data.replace('value', ''))
#			f.close()
				#-----------------------------------------------------------------

			node_list = []
			f = open(fullName, 'r')
			i=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#if(columns[3]!=str2):
				try:
			    	#	gotdata = columns[1]
					#print(columns)
					node_list.append((columns[1]))
				except IndexError:
			    		gotdata = 'null'
				#print(columns)
				#print(columns[2])
				#node_list.append((columns[1]))
				#i=i+1
				#print(columns[1])
			#	print(columns[2])
				#print(columns[3])
				#print(columns[4])
					#columns[0]	node id
					#columns[1]
					#columns[2]
					#columns[3]
					#columns[4]
					#columns[5]

 			f.close()

			node_set1 = set(node_list)
			#print len(node_set)
			#print sorted(node_set)
			total2=0
			initiators=[]
			node_list_1=[]
			node_list_2=[]
			node_list_3=[]
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'

					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (count_initiator1,count_initiator2)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group1_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t\t",round(percent_initiator2,1),"\t\t\t\t\t",count_initiator1,"\t\t\t\t\t",count_initiator2
				if (group_ini==group1_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
							
						#if(count_initiator1==total2):
						#	node_list_1.append(columns[1])
						#	count_node_1=count_node_1+1
						#if(count_initiator2==total2):
						#	node_list_2.append(columns[1])
						#	count_node_2=count_node_2+1										
						#if((count_initiator1!=total2) and (count_initiator2!=total2)):
						#	node_list_3.append(columns[1])
						#	count_node_both=count_node_both+1
					#print(columns)

			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				total2=0
				total=0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'
					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (i,count_initiator1,count_initiator2,flag)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group2_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t\t",round(percent_initiator1,1),"\t\t\t\t\t",count_initiator2,"\t\t\t\t\t",count_initiator1
				if (group_ini==group2_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
	


        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print"With Header"
        print "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print"Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t\t\t""No of packets reception from Self Group_Ini\t""No of packets reception from other GroupIni"
	f2=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/WithHeader/71332'):

		f2.extend(filenames)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Flocklab/WithHeader/71332',name)

			#print(fullName)

				#-----------------------------------------------------------------
				# For removing the NULL characters
#			f = open(fullName, 'rb')
#			data = f.read()
#			f.close()

#			f = open(fullName, 'wb')

#			f.write(data.replace('nodeid', ''))
#			f.write(data.replace('name', ''))
#			f.write(data.replace('time', ''))
#			f.write(data.replace('value', ''))
#			f.close()
				#-----------------------------------------------------------------

			node_list = []
			f = open(fullName, 'r')
			i=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#if(columns[3]!=str2):
				try:
			    	#	gotdata = columns[1]
					#print(columns)
					node_list.append((columns[1]))
				except IndexError:
			    		gotdata = 'null'
				#print(columns)
				#print(columns[2])
				#node_list.append((columns[1]))
				#i=i+1
				#print(columns[1])
			#	print(columns[2])
				#print(columns[3])
				#print(columns[4])
					#columns[0]	node id
					#columns[1]
					#columns[2]
					#columns[3]
					#columns[4]
					#columns[5]

 			f.close()

			node_set1 = set(node_list)
			#print len(node_set)
			#print sorted(node_set)
			total2=0
			initiators=[]
			node_list_1=[]
			node_list_2=[]
			node_list_3=[]
			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'

					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (count_initiator1,count_initiator2)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group1_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t\t",round(percent_initiator2,1),"\t\t\t\t\t",count_initiator1,"\t\t\t\t\t",count_initiator2
				if (group_ini==group1_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
							
						#if(count_initiator1==total2):
						#	node_list_1.append(columns[1])
						#	count_node_1=count_node_1+1
						#if(count_initiator2==total2):
						#	node_list_2.append(columns[1])
						#	count_node_2=count_node_2+1										
						#if((count_initiator1!=total2) and (count_initiator2!=total2)):
						#	node_list_3.append(columns[1])
						#	count_node_both=count_node_both+1
					#print(columns)

			for i in sorted(node_set):
				f = open(fullName, 'r')
				line_no=0;
				count_initiator1=0
				count_initiator2=0
				percent_initiator1=0.0
				percent_initiator2=0.0
				total2=0
				total=0
				flag=0
				for line in f:
					line = line.strip()
					columns = line.split()
					#print columns
					try:
					 	if (columns[1]) == i:
							#print("here")
							flag=flag+1
							gotdata = columns[1]
							#print(columns[1],columns[8])
							initiators.append((columns[22]))
							if(int(columns[22])==initiator1):
								count_initiator1=count_initiator1+1
							if(int(columns[22])==initiator2):
								count_initiator2=count_initiator2+1
					except IndexError:
						gotdata = 'null'
					#print("Here")
				initiator_set = set(initiators)
				#print (i,sorted(initiator_set))
				#print (i,count_initiator1,count_initiator2,flag)
				initiators=[]
				if(count_initiator1!=0 or count_initiator2!=0):
				#print(initiator1)
				#print(initiator2)
					total2=count_initiator1+count_initiator2
					total=float(count_initiator1+count_initiator2)
					#print(total)
					percent_initiator1=float((count_initiator1/total)*100)
        			        percent_initiator2=float((count_initiator2/total)*100)
					#print(percent_initiator1,percent_initiator2)
					group_ini=0
				for j in sorted(group1):
						#print(j,i)
					if(j==int(i)):
						group_ini=group1_initiator
				for k in sorted(group2):
					if(k==int(i)):
						group_ini=group2_initiator	
					#print(group_ini)	
				if(group_ini==group2_initiator and flag>0):
						print i,"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t\t",round(percent_initiator1,1),"\t\t\t\t\t",count_initiator2,"\t\t\t\t\t",count_initiator1
				if (group_ini==group2_initiator and flag==0):
						print i,"\t\t",group_ini,"\t\t","-------","\t\t\t\t","--------","\t\t\t\t","--------","\t\t\t\t","--------"
				
#	 			f.close()
#				node_neighbor_set = set(node_neighbor_list)
#				print i,":"
#				print sorted(node_neighbor_set)
#				node_neighbor_list = []


def main():

    if len(sys.argv) != 1:
        print '...'
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()
	
