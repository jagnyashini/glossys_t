import sys
import os
import csv

from os import walk
from fileinput import close

def analyze():

	node_count=0
	parent_list = []
	node_list_1=[]
	node_list_2=[]
	node_list_3=[]
	#group1=[2,12,7,10,9,29,34,28,30,20,17,26,18,16,51,57,61,53,62,65,46,45,48,47]
	#group2=[11,14,5,1,13,6,15,27,32,37,35,19,22,24,52,60,56,54,55,74,43,42,44,40]
	#group1=[44,38,47,46,48,18,24,19,52,56,51,54,55,30,28,35,37,15,5,13,10,14,1]
	#group2=[65,45,42,40,62,16,17,26,57,60,61,53,49,27,32,34,29,6,2,12,7,11,9]
	#group1=[56,57,60,51,54,61,55,53,62,74,45,38,42,40,6,15,29,5,2,14,11,13,12,7,1,9,10]
	#group2=[17,20,22,18,24,16,52,65,43,44,46,48,27,37,28,35,30,32,34]
	group1=[30,28,34,27,15,13,11,14,1,20,17,22,18,10,51,52,56,55,46,43,42,48,45,40]
	group2=[35,32,29,6,5,2,12,7,9,37,19,16,24,54,60,57,61,53,62,38,65,74,44,47]
	
	group1_initiator=group1[0]
	group2_initiator=group2[0]
	count_node_1=0
	count_node_2=0
	count_node_both=0
	group_ini=0
	f = []
	f2 = []
	f3=  []
	f4=[]				
	count=0
	ten=0
	not_ten=0
	nine=0
	eight=0
	seven=0
	six=0
	five=0
	four=0
	three=0
	two=0
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With SFD Change"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t""No from Self initiator\t""No from other initiator\t\t""No of times not recieved pkt"
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Downloads/S_52_30_35_result_1'):

		f.extend(filenames)
		for name in f:

			fullName = os.path.join('/home/jagnyashini/Downloads/S_52_30_35_result_1',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			zero=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				if(i>1):
					try:
						#print(columns),group1_initiator,group2_initiator 
						print(columns)
						if int(columns[4])==10 :
							ten=ten+1
						elif int(columns[4])==9 :
							nine=nine+1
						elif int(columns[4])==8 :
							eight=eight+1
						elif int(columns[4])==7 :
							seven=seven+1
						elif int(columns[4])==6 :
							six=six+1
						elif int(columns[4])==5 :
							five=five+1
						elif int(columns[4])==4 :
							four=four+1
						elif int(columns[4])==3 :
							three=three+1
						elif int(columns[4])==2 :
							two=two+1
						else:
							not_ten=not_ten+1
 						if int(columns[12])==group1_initiator:
                                                	initiator1=initiator1+1;
						if int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[12])==0:
							zero=zero+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t",round(percent_initiator2,1),"\t\t\t",initiator1,"\t\t\t",initiator2,"\t\t\t\t",zero
                	initiator1=0
                	initiator2=0
			percent_initiator1=0
			percent_initiator2=0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Downloads/S_52_30_35_result_1'):
		f2.extend(filenames2)
		for name in f2:

			fullName = os.path.join('/home/jagnyashini/Downloads/S_52_30_35_result_1',name)
			node_list = []
#			node_neighbor_list = []
			f2 = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			zero=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f2:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
						#print(columns[1],columns[22])
	 					if int(columns[12])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[12])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[12])==0:
							zero=zero+1;
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				group_ini=0
				for j in sorted(group2):
					if(j==int(columns[1])):
						group_ini=group2_initiator
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				if(group_ini==group2_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t",round(percent_initiator1,1),"\t\t\t",initiator2,"\t\t\t",initiator1,"\t\t\t\t",zero
                initiator1=0
                initiator2=0
		percent_initiator1=0
		percent_initiator2=0
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "No change"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t""No from Self initiator\t""No from other initiator\t\t""No of times not recieved pkt"
	count=0
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossys/Data/Indriya/No_change/W_44_65'):

		f4.extend(filenames)
		for name in f4:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Indriya/No_change/W_44_65',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			zero=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns),group1_initiator,group2_initiator 
 						if int(columns[10])==group1_initiator:
                                                	initiator1=initiator1+1;
						if int(columns[10])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[10])==0:
							zero=zero+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t",round(percent_initiator2,1),"\t\t\t",initiator1,"\t\t\t",initiator2,"\t\t\t\t",zero
                	initiator1=0
                	initiator2=0
			percent_initiator1=0
			percent_initiator2=0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossys/Data/Indriya/No_change/W_44_65'):
		f3.extend(filenames2)
		for name in f3:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Indriya/No_change/W_44_65',name)
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			zero=0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
				#	print(columns)
	 					if int(columns[10])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[10])==group2_initiator:
							initiator2=initiator2+1;
						if int(columns[10])==0:
							zero=zero+1;
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				group_ini=0
				for j in sorted(group2):
					if(j==int(columns[1])):
						group_ini=group2_initiator
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				if(group_ini==group2_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t",round(percent_initiator1,1),"\t\t\t",initiator2,"\t\t\t",initiator1,"\t\t\t\t",zero
                initiator1=0
                initiator2=0
		percent_initiator1=0
		percent_initiator2=0

	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "With Header"
	print "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	print "SNo\t\t" "Node Id:\t""Initiator_Id\t""Reception_from_Grp_Initiator\t""Reception_from_other\t""No from Self initiator\t""No from other initiator"
	count=0
	f5=[]
	f6=[]
	for (dirpath, dirnames, filenames) in walk('/home/jagnyashini/Code_Base/glossys/Data/Indriya/Header/H_30_35'):

		f5.extend(filenames)
		for name in f5:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Indriya/Header/H_30_35',name)
			#print fullName
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>5):
					try:
						#print(columns),group1_initiator,group2_initiator 
 						if int(columns[22])==group1_initiator:
                                                	initiator1=initiator1+1;
						if int(columns[22])==group2_initiator:
							initiator2=initiator2+1;
						group_ini=0
						for j in sorted(group1):
							if(j==int(columns[1])):
								group_ini=group1_initiator
						for j in sorted(group2):
							if(j==int(columns[1])):
								group_ini=group2_initiator
						#node_list.append(columns[1])
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				if(group_ini==group1_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator1,1),"\t\t\t\t",round(percent_initiator2,1),"\t\t\t",initiator1,"\t\t\t",initiator2
                	initiator1=0
                	initiator2=0
			percent_initiator1=0
			percent_initiator2=0
	for (dirpath, dirnames, filenames2) in walk('/home/jagnyashini/Code_Base/glossys/Data/Indriya/Header/H_30_35'):
		f6.extend(filenames2)
		for name in f6:

			fullName = os.path.join('/home/jagnyashini/Code_Base/glossys/Data/Indriya/Header/H_30_35',name)
			node_list = []
#			node_neighbor_list = []
			f = open(fullName, 'r')
			line_no=0
			i=0
			rx_cnt=0
			initiator1=0
			initiator2=0
			percent_initiator1=0.0
			percent_initiator2=0.0
			for line in f:
				line = line.strip()
				#print(line)
				columns = line.split()
				#print(columns)
				if(i>1):
					try:
				#	print(columns)
	 					if int(columns[22])==group1_initiator:
	                                                initiator1=initiator1+1;
						if int(columns[22])==group2_initiator:
							initiator2=initiator2+1;
					except (IndexError,ValueError):
						gotdata = 'null'
				i=i+1
			node_set = set(node_list)
#			print len(node_set)
			#print(node_set)
			if(initiator1!=0 or initiator2!=0):
				#print(initiator1)
				#print(initiator2)
				total2=initiator1+initiator2
				total=float(initiator1+initiator2)
				#print(total)
				percent_initiator1=float((initiator1/total)*100)
        	                percent_initiator2=float((initiator2/total)*100)
				group_ini=0
				for j in sorted(group2):
					if(j==int(columns[1])):
						group_ini=group2_initiator
				for i in sorted(group1):
					if(i==int(columns[1])):
						group_ini=group1_initiator
				if(group_ini==group2_initiator):
					count=count+1
					print count,"\t\t",columns[1],"\t\t",group_ini,"\t\t",round(percent_initiator2,1),"\t\t\t\t",round(percent_initiator1,1),"\t\t\t",initiator2,"\t\t\t",initiator1
                initiator1=0
                initiator2=0
		percent_initiator1=0
		percent_initiator2=0
		print "Recieved 10 times:",ten
		print "Recieved 9 times:",nine
		print "Recieved 8 times:",eight
		print "Recieved 7 times:",seven
		print "Recieved 6 times:",six
		print "Recieved 5 times:",five
		print "Recieved 4 times:",four
		print "Recieved 3 times:",three
		print "Recieved 2 times:",two
		print "Recieved 1,0 times:",not_ten
		
		
def main():

    if len(sys.argv) != 1:
        print('...')
        sys.exit(1)

    analyze();

if __name__ == '__main__':
  main()

